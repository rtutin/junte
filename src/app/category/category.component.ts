import { Component, OnInit } from '@angular/core';
import { BackendService } from '../backend.service';
import { ActivatedRoute } from '@angular/router';
import {CartService} from '../cart.service';


@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {
  model = {
    title: '',
    list: []
  };

  constructor(
      private service: BackendService,
      private route: ActivatedRoute,
      private cart: CartService
  ) { }

  ngOnInit() {
      this.route.params.subscribe(params => {
          this.service.loadCategory(+params.id).subscribe((r: any) => {
              this.model = {
                  title: r.title,
                  list: r.list
              };
          });
      });
  }

  add(item) {
      this.cart.add(item);
  }
}
