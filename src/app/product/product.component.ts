import { Component, OnInit } from '@angular/core';
import {BackendService} from '../backend.service';
import {ActivatedRoute} from '@angular/router';
import {CartService} from '../cart.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  model;

  constructor(private service: BackendService, private route: ActivatedRoute, private cart: CartService) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.service.loadProduct(+params.id).subscribe((r: any) => {
        this.model = r;
      });
    });
  }

  add(item) {
    this.cart.add(item);
  }
}
