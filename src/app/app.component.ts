import {Component, OnInit} from '@angular/core';
import { BackendService } from './backend.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  data;

  constructor(private service: BackendService, private router: Router) {}

  ngOnInit() {
    this.service.loadCategories().subscribe((r: any) => {
      this.data = r.list;
    });
  }

  move(route) {
    this.router.navigate([route]);
  }
}
