import { Component, OnInit } from '@angular/core';
import {CartService} from '../cart.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  constructor(public cart: CartService) { }

  ngOnInit() {
  }

  add(item) {
    this.cart.add(item);
  }

  subtract(item) {
    this.cart.subtract(item);
  }

  remove(item) {
    this.cart.remove(item);
  }

  order() {
    alert('Ваш заказ принят');
  }
}
