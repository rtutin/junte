import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  list = [];
  summary = 0;

  constructor() { }

  add(item) {
    if (this.list[item.id]) {
      this.list[item.id] = {
        id: item.id,
        name: item.name,
        price: item.price,
        count: this.list[item.id].count + 1
      };
    } else {
      this.list[item.id] = {
        id: item.id,
        name: item.name,
        price: item.price,
        count: 1
      };
    }

    this.calcSummary();
  }

  subtract(item) {
    this.list[item.id].count--;

    if (this.list[item.id].count === 0) {
      this.list[item.id] = null;
    }

    this.calcSummary();
  }

  remove(item) {
    this.list[item.id] = null;
    this.calcSummary();
  }

  calcSummary() {
    let result = 0;

    for (const item of this.list) {
      if (item && item !== null) {
        result = result + (item.count * item.price);
      }
    }

    this.summary = result;
  }
}
