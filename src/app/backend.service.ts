import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class BackendService {
  constructor(private http: HttpClient) {}

  loadCategories() {
    return this.http.get('/assets/data/categories.json');
  }

  loadCategory(id: number) {
    return this.http.get('/assets/data/category-' + id + '.json');
  }

  loadProduct(id: number) {
    return this.http.get('/assets/data/product-' + id + '.json');
  }
}
